<?php

require_once 'config/database.php';

class ModelMailBox {
    
    public $conn = '';

    public function __construct() {
        $this->conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
		
		if($this->conn->connect_error) {
			die('Connection failed:'. $this->conn->connect_error);
		}
    }
}

?>