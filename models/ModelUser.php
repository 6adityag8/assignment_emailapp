<?php

require_once 'config/database.php';

class ModelUser {
	
	public $conn = '';
	
	public function __construct() {
		$this->conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
		
		if($this->conn->connect_error) {
			die('Connection failed:'. $this->conn->connect_error);
		}
	}
	
	public function insert($table_name, $data) {
		$string = "INSERT INTO $table_name (";
		$string .= implode(",", array_keys($data)) . ') VALUES (';
		$string .= "'" . implode("','", array_values($data)) . "')";
		if($this->conn->query($string)) {
			return true;  
		}
		else {
			echo $this->conn->error();  
		}
	}
	
	public function select($table_name, $where_condition) {
		$array     = [];
		$condition = '';
		
		foreach($where_condition as $key => $value) {
			$condition .="$key='$value' AND ";
		}
		$condition = substr($condition, 0, -5);
		$string  = "SELECT * FROM $table_name WHERE $condition";
		$result  = $this->conn->query($string);
		
		while($row = $result->fetch_assoc()) {
			$array[] = $row;
		}
		return $array;
	}
	
	public function update($table_name, $fields, $where_condition) {
		$query     = '';
		$condition = '';
		
		foreach($fields as $key => $value) {
			$query .= "$key='$value', ";
		}
		$query = substr($query, 0, -2); 
		
		foreach($where_condition as $key => $value) {
			$condition .= "$key='$value' AND ";
		}
		$condition = substr($condition, 0, -5);
		$query = "UPDATE $table_name SET $query WHERE $condition";
		
		if($this->conn->query($query)) {
			return true;
		}
		
		else {
			return false;
		}
	}
	public function drop($table_name, $where_condition) {
		$condition = '';
		
		foreach($where_condition as $key => $value) {
			$condition .="$key='$value' AND ";
		}
		$condition = substr($condition, 0, -5);
		$string  = "DELETE FROM $table_name WHERE $condition";
		$result  = $this->conn->query($string);
		
		if ($result) {
			return true;
		}
		
		else {
			return false;
		}
	}
}