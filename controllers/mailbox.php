<?php

class mailbox extends Controller {

	public function __construct() {
		session_start();

		if(empty($_SESSION['user'])) {
			header('location: /index.php/user/login');
			$id = $_SESSION['user'];
		}
		$db = $this->model('ModelUser');
	}

	public function inbox() {
		$result  	  = $db->select('email',['to_id' => $id]);
		$sub     	  = [];
		$from    	  = [];
		$from_id 	  = [];
		$created_time = [];

		foreach($result as $row) {
			$sub     	  = $row['subject'];
			$query   	  = $db->select('user',['id' => $row['from_id']]);
			$from    	  = $query[0]['name'];
			$from_id 	  = $row['from_id'];
			$created_time = $row['created_time'];
		}
		$data = [$sub,$from,$from_id];
		$this->view('mailbox/inbox', $data);
	}

	public function sent_mail() {
		$result  	  = $db->select('email',['from_id' => $id]);
		$sub     	  = [];
		$to   	  	  = [];
		$to_id 	  	  = [];
		$created_time = [];

		foreach($result as $row) {
			$sub     	  = $row['subject'];
			$query   	  = $db->select('user',['id' => $row['to_id']]);
			$to    	      = $query[0]['name'];
			$to_id 	      = $row['from_id'];
			$created_time = $row['created_time'];
		}
		$data = [$sub,$from,$from_id];
		$this->view('mailbox/sent_mail', $data);
	}

	public function read_mail($param1, $param2) {
		$read_mail_id   = $param1;
		$read_mail_time = $param2;
		$read_result    = $db->select('email',['to_id'        => $id,
										       'from_id' 	  => $read_mail_id,
										       'created_time' => $read_mail_time
											  ]);
		$read_query     = $db->select('user',['id' => $read_result[0]['from_id']]);
		$read_mail_from = $read_query[0]['name'];											 
		$data 			= [$result,$read_mail_from];
		$this->view('mailbox/read_mail', $data);
	}

	public function compose() {

		if (isset($_POST["subject"])) {
			$subject = $db->conn->real_escape_string($_POST["subject"]);
		}

		if (isset($_POST["body"])) {
			$body = $db->conn->real_escape_string($_POST["body"]);
		}

		if (isset($_POST["to"])) {
			$to = $db->conn->real_escape_string($_POST["to"]);
		}

		if (isset($_POST["send"])) {
			$send = $_POST["send"];
		}

		if (isset($send)) {
			$compose_row    = $db->select('user',['email' => $to]);
			$to_id          = $compose_row[0]['id'];
			$compose_data   = [
								'subject' => $subject,
								'body'    => $body,
								'from_id' => $id,
								'to_id'   => $to_id
							  ];
			$compose_result = $db->insert('email', $compose_data);

			if ($compose_result) {
				header('location: /index.php/mailbox/inbox');
			}
		}
		$this->view('mailbox/compose');
	}
}