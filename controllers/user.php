<?php

session_start();

class user extends Controller {
	
	public function __construct() {
		$db = $this->model('ModelUser');
		
		if (isset($_POST["login"])) {
			$login = $_POST["login"];
		}

		if (isset($_POST["register"])) {
			$register = $_POST["register"];
		}

		if (isset($_POST["name"])) {
			$name = $db->conn->real_escape_string($_POST["name"]);
		}
		
		if (isset($_POST["email"])) {
			$email = $db->conn->real_escape_string($_POST["email"]);
		}
		
		if (isset($_POST["password"])) {
			$password  = $db->conn->real_escape_string($_POST["password"]);
			//salted the password
			$password = "sdb6734672ydbsvd78" . $password;
			//hashed the salted password
			$password = md5($password);
		}
		
		if (isset($_POST["confirm_password"])) {
			$confirm_password  = $db->conn->real_escape_string($_POST["confirm_password"]);
			$confirm_password = "sdb6734672ydbsvd78" . $confirm_password;
			$confirm_password = md5($confirm_password);
		}
	}
	
	public function register() {
		
		if(!empty($_SESSION['user'])) {
			header('location: /index.php/mailbox/inbox');
		}

		if(isset($register)) {
			$data = [
					'name'     => $name,
					'email'    => $email,
					'password' => $password,
					];
			$result = $db->insert('user', $data);
			// if the query runs successfully, the data is stored into the DB and the user is redirected to the login page
			if($result) {
				header('location: /index.php/user/login');
			}
		}

		else {
			unset($_SESSION);
			$this->view('auth/register');
		}
	}

	public function login() {
		
		if(!empty($_SESSION['user'])) {
			header('location: /index.php/mailbox/inbox');
		}

		if(isset($login)) {
			$row = $db->select('user',['email'    => $email,
									   'password' => $password]);
			$numrows = sizeof($row);
			// if the number of rows isn't 0, then the user has already registered
			if (!empty($numrows)) {				
				// session starts and email is stored into the session variable for further session validations
				$_SESSION['user'] = $row[0]['id'];
				// on successful login, user gets redirected to dashboard page
				header('location: /index.php/mailbox/inbox');
			}
			// if the number of rows is 0, then user hasn't registered yet
			else {
				echo "<script>alert('Invalid Username or password, Try Again!');</script>";
			}
		}

		else {
			unset($_SESSION);
			$this->view('auth/login');
		}
	}

	public function logout() {
		unset($_SESSION);
		session_destroy();
		header('location: /index.php/user/login');
	}
}