<?php

class Bootstrap {

	public $controller = 'mailbox';

	public $method = 'inbox';

	public $param = [];

	function __construct() {

		$url = $_SERVER['REQUEST_URI'];
		$url = explode(".php/",$url);
		$url = rtrim($url[1], '/');
		$url = explode('/', $url);		
		$file = 'controllers/' . $url[0] . '.php';

		if (file_exists($file)) {
			$this->controller = $url[0];
			unset($url[0]);
		}
		require_once 'controllers/' . $this->controller . '.php';
		$this->controller = new $this->controller;
		// calling methods
		if(method_exists($this->controller, $url[1]))
		{
			$this->method = $url[1];
			unset($url[1]);
		}

		$this->param = $url ? array_values($url) : [];

		call_user_func_array([$this->controller, $this->method], $this->param);
	}
}

?>