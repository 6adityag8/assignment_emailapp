<?php
include dirname(__DIR__) . '/layout/header/mailbox.php';
?>
<section class="content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
        <a href="/index.php/mailbox/inbox" class="btn btn-primary btn-block mb-3">Back to Inbox</a>

        <div class="card">
            <div class="card-header">
            <h3 class="card-title">Folders</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            </div>
            <div class="card-body p-0">
            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                <a href="/index.php/mailbox/inbox" class="nav-link">
                    <i class="fa fa-inbox"></i> Inbox
                    <span class="badge bg-primary float-right">12</span>
                </a>
                </li>
                <li class="nav-item">
                <a href="/index.php/mailbox/sent_mail" class="nav-link">
                    <i class="fa fa-envelope-o"></i> Sent
                </a>
                </li>
            </ul>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /. box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
        <div class="card card-primary card-outline">
            <div class="card-header">
            <h3 class="card-title">Compose New Message</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <form action="/index.php/mailbox/compose" method="POST" role="form">
            <div class="form-group">
                <input class="form-control" name="to" placeholder="To:">
            </div>
            <div class="form-group">
                <input class="form-control" name="subject" placeholder="Subject:">
            </div>
            <div class="form-group">
                <textarea id="compose-textarea" name="body" class="form-control" style="height: 300px">                     
                </textarea>
            </div>
            <div class="form-group">
                <div class="btn btn-default btn-file">
                <i class="fa fa-paperclip"></i> Attachment
                <input type="file" name="attachment">
                </div>
                <p class="help-block">Max. 32MB</p>
            </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
            <div class="float-right">
                <button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Draft</button>
                <input type="submit" name="send" value="Send" class="btn btn-primary"></input>
            </div>
            <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>
            </div>
            </form>
            <!-- /.card-footer -->
        </div>
        <!-- /. box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
<?php
include dirname(__DIR__) . '/layout/footer/mailbox.php';
?>