<?php
include dirname(__DIR__) . '/layout/header/mailbox.php';
?>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
        <a href="/index.php/mailbox/inbox" class="btn btn-primary btn-block mb-3">Back to Inbox</a>

        <div class="card">
            <div class="card-header">
            <h3 class="card-title">Folders</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            </div>
            <div class="card-body p-0">
            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                <a href="mailbox.php" class="nav-link">
                    <i class="fa fa-inbox"></i> Inbox
                    <span class="badge bg-primary float-right">12</span>
                </a>
                </li>
                <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="fa fa-envelope-o"></i> Sent
                </a>
                </li>
            </ul>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /. box -->
        </div>
        <!-- /.col -->
    <div class="col-md-9">
        <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title">Read Mail</h3>

            <div class="card-tools">
            <a href="#" class="btn btn-tool" data-toggle="tooltip" title="Previous"><i class="fa fa-chevron-left"></i></a>
            <a href="#" class="btn btn-tool" data-toggle="tooltip" title="Next"><i class="fa fa-chevron-right"></i></a>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body p-0">
            <div class="mailbox-read-info">
            <h5><?php echo $data[0][0]['subject']; ?></h5>
            <h6>From: <?php echo $data[1]; ?>
                <span class="mailbox-read-time float-right"><?php echo $data[0][0]['created_time']; ?></span></h6>
            </div>
            <!-- /.mailbox-read-info -->
            <div class="mailbox-controls with-border text-center">
            <div class="btn-group">
                <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Delete">
                <i class="fa fa-trash-o"></i></button>
                <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Reply">
                <i class="fa fa-reply"></i></button>
                <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Forward">
                <i class="fa fa-share"></i></button>
            </div>
            <!-- /.btn-group -->
            <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Print">
                <i class="fa fa-print"></i></button>
            </div>
            <!-- /.mailbox-controls -->
            <div class="mailbox-read-message">
            <?php echo $data[0][0]['body']; ?>
            </div>
            <!-- /.mailbox-read-message -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <div class="float-right">
            <button type="button" class="btn btn-default"><i class="fa fa-reply"></i> Reply</button>
            <button type="button" class="btn btn-default"><i class="fa fa-share"></i> Forward</button>
            </div>
            <button type="button" class="btn btn-default"><i class="fa fa-trash-o"></i> Delete</button>
            <button type="button" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
        </div>
        <!-- /.card-footer -->
        </div>
        <!-- /. box -->
    </div>
    <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<?php
include dirname(__DIR__) . '/layout/footer/mailbox.php';
?>