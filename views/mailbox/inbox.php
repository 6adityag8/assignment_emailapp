<?php
include dirname(__DIR__) . '/layout/header/mailbox.php';
?>
<!-- Main content -->
<section class="content">
    <div class="row">
    <div class="col-md-3">
        <a href="/index.php/mailbox/compose" class="btn btn-primary btn-block mb-3">Compose</a>

        <div class="card">
        <div class="card-header">
            <h3 class="card-title">Folders</h3>

            <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            </div>
        </div>
        <div class="card-body p-0">
            <ul class="nav nav-pills flex-column">
            <li class="nav-item active">
                <a href="#" class="nav-link">
                <i class="fa fa-inbox"></i> Inbox
                <span class="badge bg-primary float-right">12</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                <i class="fa fa-envelope-o"></i> Sent
                </a>
            </li>
            </ul>
        </div>
        <!-- /.card-body -->
        </div>
        <!-- /. box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
        <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title">Inbox</h3>

            <div class="card-tools">
            <div class="input-group input-group-sm">
                <input type="text" class="form-control" placeholder="Search Mail">
                <div class="input-group-append">
                <div class="btn btn-primary">
                    <i class="fa fa-search"></i>
                </div>
                </div>
            </div>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        <div class="card-body p-0">
            <div class="mailbox-controls">
            <!-- Check all button -->
            <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
            </button>
            <div class="btn-group">
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
            </div>
            <!-- /.btn-group -->
            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
            <div class="float-right">
                1-50/200
                <div class="btn-group">
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                </div>
                <!-- /.btn-group -->
            </div>
            <!-- /.float-right -->
            </div>
            <div class="table-responsive mailbox-messages">
            <table class="table table-hover table-striped">
                <tbody>
                <?php
                $numrows = sizeof($data[0]);
                for ($x = 0; $x < $numrows; $x++) {
                ?>
                    <tr>
                    <td><input type="checkbox"></td>
                    <td class="mailbox-star"><a href="#"><i class="fa fa-star text-warning"></i></a></td>
                    <td class="mailbox-name"><a href="/index.php/mailbox/read_mail/<?php echo $data[2][$x] . "/" . $data[3][$x]; ?>"><?php echo $data[1][$x]; ?></a></td>
                    <td class="mailbox-subject"><b><?php echo $data[0][$x]; ?></b></td>
                    </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
            <!-- /.table -->
            </div>
            <!-- /.mail-box-messages -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer p-0">
            <div class="mailbox-controls">
            <!-- Check all button -->
            <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
            </button>
            <div class="btn-group">
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
            </div>
            <!-- /.btn-group -->
            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
            <div class="float-right">
                1-50/200
                <div class="btn-group">
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                </div>
                <!-- /.btn-group -->
            </div>
            <!-- /.float-right -->
            </div>
        </div>
        </div>
        <!-- /. box -->
    </div>
    <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<?php
include dirname(__DIR__) . '/layout/footer/mailbox.php';
?>