<?php
include dirname(__DIR__) . '/layout/header/auth.php';
?>
<div class="card-body register-card-body">
  <p class="login-box-msg">Register a new membership</p>

  <form action="/index.php/user/register" method="post">
    <div class="form-group has-feedback">
      <input type="text"  class="form-control" name="name" placeholder="Full name">
      <span class="fa fa-user form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
      <input type="email" class="form-control" name="email" placeholder="Email">
      <span class="fa fa-envelope form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
      <input type="password" class="form-control" name="password" placeholder="Password">
      <span class="fa fa-lock form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
      <input type="password" class="form-control" name="confirm_password" placeholder="Retype password">
      <span class="fa fa-lock form-control-feedback"></span>
    </div>
    <div class="row">
      <div class="col-8">
        <div class="checkbox icheck">
          <label>
            <input type="checkbox"> I agree to the <a href="#">terms</a>
          </label>
        </div>
      </div>
      <!-- /.col -->
      <div class="col-4">
        <input type="submit" value="Register" name="register" class="btn btn-primary btn-block btn-flat"></input>
      </div>
      <!-- /.col -->
    </div>
  </form>

  <a href="/index.php/user/login" class="text-center">I already have a membership</a>
</div>
<?php
include dirname(__DIR__) . '/layout/footer/auth.php';
?>