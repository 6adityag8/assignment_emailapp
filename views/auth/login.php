<?php
include dirname(__DIR__) . '/layout/header/auth.php';
?>
<div class="card-body login-card-body">
<p class="login-box-msg">Sign in to start your session</p>

<form action="/index.php/user/login" method="post">
  <div class="form-group has-feedback">
    <input type="email" name="email" class="form-control" placeholder="Email">
    <span class="fa fa-envelope form-control-feedback"></span>
  </div>
  <div class="form-group has-feedback">
    <input type="password" name="password" class="form-control" placeholder="Password">
    <span class="fa fa-lock form-control-feedback"></span>
  </div>
  <div class="row">
    <div class="col-8">
      <div class="checkbox icheck">
        <label>
          <input type="checkbox"> Remember Me
        </label>
      </div>
    </div>
    <!-- /.col -->
    <div class="col-4">
      <input type="submit" value="Sign In" name="login" class="btn btn-primary btn-block btn-flat"></input>
    </div>
    <!-- /.col -->
  </div>
</form>
<!-- /.social-auth-links -->

<p class="mb-1">
  <a href="#">I forgot my password</a>
</p>
<p class="mb-0">
  <a href="/index.php/user/register" class="text-center">Register a new membership</a>
</p>
</div>
<?php
include dirname(__DIR__) . '/layout/footer/auth.php';
?>
