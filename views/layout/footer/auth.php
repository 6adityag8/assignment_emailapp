    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="<?php echo VIEW_HEADER_URL; ?>/js/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo VIEW_HEADER_URL; ?>/js/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- iCheck -->
<script src="<?php echo VIEW_HEADER_URL; ?>/js/plugins/iCheck/icheck.min.js"></script>
<!-- custom -->
<script src="<?php echo VIEW_HEADER_URL; ?>/js/custom.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    })
  })
</script>
</body>
</html>
