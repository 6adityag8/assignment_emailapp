</div>
  <!-- /.content-wrapper -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo VIEW_HEADER_URL; ?>/js/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo VIEW_HEADER_URL; ?>/js/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo VIEW_HEADER_URL; ?>/js/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo VIEW_HEADER_URL; ?>/js/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo VIEW_HEADER_URL; ?>/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo VIEW_HEADER_URL; ?>/js/demo.js"></script>
<!-- custom -->
<script src="<?php echo VIEW_HEADER_URL; ?>/js/custom.js"></script>
</body>
</html>
